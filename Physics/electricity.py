import sys

def parallelResistance(resistance):
    number = float();
    for i in range(0, len(resistance)):
        number = number + 1.0/resistance[i]       

    result = 1.0/number
    return result


print(str(parallelResistance([5,5,5])));
