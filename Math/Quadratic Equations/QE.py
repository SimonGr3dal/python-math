# This script was created by Mathias Jensen (c) 2013 (mjdk.dk)

import sys
import math

def topCoordinates(a,b,c, D):
    top = [(-b)/(2*a), (-D)/(4*a)]
    print("Top: " + str(top))
    return top

def calculate(a,b,c):
    a = float(a)
    b = float(b)
    c = float(c)
    D = (b*b) - (4*a*c)

    topCoordinates(a,b,c,D)
    print("D = " + str(D))
    if D == 0:
        # One solution
        x = (-b + math.sqrt(D)) / (2*a)
        return x
        
    elif D > 0:
        # More than one solution
        x1 = (-b + math.sqrt(D)) / (2*a)
        x2 = (-b - math.sqrt(D)) / (2*a)
        answers = [x1, x2]
        return answers
        
    else:
        return None

def userCalculate():
    print("ax^2 + bx + c\nPlease feed me the numbers:\n")
    
    try:
        a = float(raw_input("a: "))
        b = float(raw_input("b: "))
        c = float(raw_input("c: "))

        value = calculate(a,b,c)
        if value == None:
        	D = (b*b) - (4*a*c)
        	print("There is no solution as D equals " + str(D))
    	else:
        	print(str(value))
    	raw_input();


    except ValueError:
        print("You have to enter three numbers.\nTry again:\n")
    
print(str(calculate(-2, 40, 0)))
#while True:
	#userCalculate()