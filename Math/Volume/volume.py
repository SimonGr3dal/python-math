import sys
import math

def ball(r):
    r = float(r)
    # 4/3 * pi * r^3
    answer = (4.0/3.0) * math.pi * (r*r*r)
    return answer

def testBall():
    try:
        r = float(raw_input("Radius: "))
    except ValueError:
        print("You have to enter a number")
    print(str(ball(r)))

#testBall()

def cone(r, h):
    r = float(r)
    h = float(h)
    # 1/3 * pi * h * r^2
    answer = (1.0/3.0) * math.pi * h * (r*r)
    return answer

def pyramid(b, h):
    b = float(b)
    h = float(h)
    # 1/3 * base area * height
    answer = (1.0/3.0) * b * h
    return answer

def flatPyramid(h, bigB, smallB):
    h = float(h)
    bigB = float(bigB)
    smallB = float(smallB)
    if bigB > smallB:
        answer = (h/3.0) * ((bigB + smallB) + math.sqrt(bigB * smallB))
        return answer
    else:
        return None
